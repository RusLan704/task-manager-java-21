package ru.bakhtiyarov.tm.api.locator;

import ru.bakhtiyarov.tm.api.service.ICommandService;

public interface ServiceLocator {

    ICommandService getCommandService();

}
