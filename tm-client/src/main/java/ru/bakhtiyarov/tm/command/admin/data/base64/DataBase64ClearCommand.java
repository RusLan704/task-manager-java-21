package ru.bakhtiyarov.tm.command.admin.data.base64;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.command.AbstractCommand;

public final class DataBase64ClearCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-base64-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear base64 file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 CLEAR]");
        endpointLocator.getAdminDataEndpoint().clearBase64(session);
        System.out.println("[OK]");
    }

}
