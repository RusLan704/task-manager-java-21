package ru.bakhtiyarov.tm.command.admin.data.base64;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.command.AbstractCommand;

public final class DataBase64LoadCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-base64-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load base64 data from file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 LOAD]");
        endpointLocator.getAdminDataEndpoint().loadBase64(session);
        System.out.println("[OK]");
    }

}
