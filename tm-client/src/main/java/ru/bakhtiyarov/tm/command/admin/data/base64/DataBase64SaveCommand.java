package ru.bakhtiyarov.tm.command.admin.data.base64;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.command.AbstractCommand;

public final class DataBase64SaveCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-base64-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to base64 file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 SAVE]");
        endpointLocator.getAdminDataEndpoint().saveBase64(session);
        System.out.println("[OK]");
    }

}
