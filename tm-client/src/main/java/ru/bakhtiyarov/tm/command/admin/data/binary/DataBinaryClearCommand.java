package ru.bakhtiyarov.tm.command.admin.data.binary;

import ru.bakhtiyarov.tm.command.AbstractCommand;

public final class DataBinaryClearCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-bin-clear";
    }


    @Override
    public String description() {
        return "Clear bin file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BIN CLEAR]");
        endpointLocator.getAdminDataEndpoint().clearBinary(session);
        System.out.println("[OK]");
    }

}


