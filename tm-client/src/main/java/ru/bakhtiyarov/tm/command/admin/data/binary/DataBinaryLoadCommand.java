package ru.bakhtiyarov.tm.command.admin.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.AbstractCommand;

public final class DataBinaryLoadCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-bin-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from binary file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BINARY LOAD]");
        endpointLocator.getAdminDataEndpoint().loadBinary(session);
        System.out.println("[OK]");
    }

}