package ru.bakhtiyarov.tm.command.admin.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.AbstractCommand;

public final class DataBinarySaveCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-bin-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data from binary file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BINARY SAVE]");
        endpointLocator.getAdminDataEndpoint().saveBinary(session);
        System.out.println("[OK]");
    }

}