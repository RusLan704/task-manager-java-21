package ru.bakhtiyarov.tm.command.admin.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.AbstractCommand;

public final class DataJsonClearCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-json-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear json file.";
    }

    @NotNull
    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON CLEAR]");
        endpointLocator.getAdminDataEndpoint().clearJson(session);
        System.out.println("[OK]");
    }

}