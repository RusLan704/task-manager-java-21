package ru.bakhtiyarov.tm.command.admin.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.AbstractCommand;

public final class DataJsonSaveCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-json-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data from json file.";
    }

    @NotNull
    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON SAVE]");
        endpointLocator.getAdminDataEndpoint().saveJson(session);
        System.out.println("[OK]");
    }

}
