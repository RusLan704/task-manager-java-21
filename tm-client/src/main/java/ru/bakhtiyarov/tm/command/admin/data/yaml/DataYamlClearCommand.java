package ru.bakhtiyarov.tm.command.admin.data.yaml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.AbstractCommand;

public final class DataYamlClearCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-yaml-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear yaml file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA YAML CLEAR]");
        endpointLocator.getAdminDataEndpoint().clearYaml(session);
        System.out.println("[OK]");
    }

}