package ru.bakhtiyarov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.endpoint.Session;
import ru.bakhtiyarov.tm.endpoint.SessionEndpoint;

public final class LogoutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @NotNull
    @Override
    public String description() {
        return "Logout user in program";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        @NotNull final SessionEndpoint sessionEndpoint = endpointLocator.getSessionEndpoint();
        sessionEndpoint.closeSessionAll(session);
        session = new Session();
        System.out.println("[OK]");
    }

}
