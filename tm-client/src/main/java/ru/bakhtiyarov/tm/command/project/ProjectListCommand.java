package ru.bakhtiyarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.endpoint.Project;
import ru.bakhtiyarov.tm.endpoint.ProjectEndpoint;

import java.util.List;

public final class ProjectListCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return TerminalConst.PROJECT_LIST;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list.";
    }

    @Override
    public void execute() {
        System.out.println("[LIST PROJECT]");
        @NotNull final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        @NotNull final List<Project> projects = projectEndpoint.findAllProjectsByUserId(session);
        int index = 1;
        for (@NotNull final Project project : projects) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
        }
        System.out.println("[OK]");
    }

}