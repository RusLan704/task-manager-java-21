package ru.bakhtiyarov.tm.exception.user;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class AccessForbiddenException  extends AbstractException {

    @NotNull
    public AccessForbiddenException() {
        super("Error! Access forbidden...");
    }

}