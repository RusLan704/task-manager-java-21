package ru.bakhtiyarov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.entity.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void addAll(@NotNull List<E> records);

    @Nullable
    E add(@NotNull E record);

    void clearAll();

    @Nullable
    E removeById(@Nullable String id);

    @NotNull
    List<E> findAll();

    @Nullable
    E findById(@NotNull String id);

    @Nullable
    E remove(@NotNull E record);

}
