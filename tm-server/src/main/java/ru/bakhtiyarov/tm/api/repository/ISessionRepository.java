package ru.bakhtiyarov.tm.api.repository;

import ru.bakhtiyarov.tm.entity.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    List<Session> findAll(String userId);

    void removeByUserId(String userId);

}
